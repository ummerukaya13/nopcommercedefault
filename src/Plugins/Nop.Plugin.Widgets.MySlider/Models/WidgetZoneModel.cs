﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.MySlider.Models
{
    public class WidgetZoneModel
    {
        public int WidgetId { get; set; }

        public string WidgetName { get; set; }
    }
}
