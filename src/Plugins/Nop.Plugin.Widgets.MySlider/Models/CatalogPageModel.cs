﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.MySlider.Models
{
    public class CatalogPageModel
    {
        public int CatalogPageId { get; set; }
        public string CatalogPageName { get; set; }
    }
}
