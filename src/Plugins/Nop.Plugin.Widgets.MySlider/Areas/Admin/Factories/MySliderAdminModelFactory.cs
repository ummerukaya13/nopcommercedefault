﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Plugin.MySlider.Domains;
using Nop.Plugin.Widgets.MySlider.Areas.Admin.Models;
using Nop.Plugin.Widgets.MySlider.Domains;
using Nop.Plugin.Widgets.MySlider.Helpers;
using Nop.Plugin.Widgets.MySlider.Services;
using Nop.Services.Configuration;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Widgets.MySlider.Areas.Admin.Factories
{
    public class MySliderAdminModelFactory : IMySliderAdminModelFactory
    {
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingSupportedModelFactory _storeMappingSupportedModelFactory;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IMySliderCustomerService _mysliderCustomerService;
        private readonly IMySliderService _mysliderService;
        private readonly IAclSupportedModelFactory _aclSupportedModelFactory;

        public MySliderAdminModelFactory(IStoreContext storeContext,
            IStoreMappingSupportedModelFactory storeMappingSupportedModelFactory,
            ILocalizedModelFactory localizedModelFactory,
            IBaseAdminModelFactory baseAdminModelFactory,
            ILocalizationService localizationService,
            IMySliderCustomerService mysliderCustomerService,
            IPictureService pictureService,
            ISettingService settingService,
            IDateTimeHelper dateTimeHelper,
            IMySliderService mysliderService,
            IAclSupportedModelFactory aclSupportedModelFactory)
        {
            _storeContext = storeContext;
            _storeMappingSupportedModelFactory = storeMappingSupportedModelFactory;
            _localizedModelFactory = localizedModelFactory;
            _baseAdminModelFactory = baseAdminModelFactory;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _settingService = settingService;
            _dateTimeHelper = dateTimeHelper;
            _mysliderService = mysliderService;
            _aclSupportedModelFactory = aclSupportedModelFactory;
            _mysliderCustomerService = mysliderCustomerService;
        }

        public async Task<ConfigurationModel> PrepareConfigurationModelAsync()
        {
            var storeId = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var sliderSettings = await _settingService.LoadSettingAsync<MySliderSettings>(storeId);

            var availableCustomerRoles = new List<SelectListItem>();
            await PrepareCustomCustomerRolesAsync(availableCustomerRoles, true);

            var model = sliderSettings.ToSettingsModel<ConfigurationModel>();
           

            model.ActiveStoreScopeConfiguration = storeId;
            model.AvailableCustomerRoles = availableCustomerRoles;
            model.SelectedCustomerRoleIds = MySliderHelper.GetGlobalCustomerRoleIds(sliderSettings.SelectedCustomerRoleIds);
            
            if (storeId <= 0)
                return model;

            model.EnableSlider_OverrideForStore = await _settingService.SettingExistsAsync(sliderSettings, x => x.EnableSlider, storeId);
            model.SelectedCustomerRoleIds_OverrideForStore = await _settingService.SettingExistsAsync(sliderSettings, x => x.SelectedCustomerRoleIds, storeId);

            return model;
        }


        //Slider Item Model
        public async Task<MySliderItemModel> PrepareSliderItemModelAsync(MySliderItemModel model, MySliders slider, MySliderItem sliderItem, bool excludeProperties = false)
        {
            
            if (sliderItem != null)
            {
                if (model == null)
                {
                    model = sliderItem.ToModel<MySliderItemModel>();
                    model.PictureUrl = await _pictureService.GetPictureUrlAsync(sliderItem.PictureId, 200);
                    model.FullPictureUrl = await _pictureService.GetPictureUrlAsync(sliderItem.PictureId);
                    model.MobilePictureUrl = await _pictureService.GetPictureUrlAsync(sliderItem.MobilePictureId, 200);
                    model.MobileFullPictureUrl = await _pictureService.GetPictureUrlAsync(sliderItem.MobilePictureId);
                    model.SliderItemTitle = sliderItem.Title;
                }

            }

            model.MySlidersId = slider.Id;

            return model;
        }


        //slider item list model
        public async Task<MySliderItemListModel> PrepareSliderItemListModelAsync(MySliderItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));


            var sliderItems = await _mysliderService.GetSliderItemsBySliderIdAsync(searchModel.SliderId, searchModel.Page - 1, searchModel.PageSize);


            var model = await new MySliderItemListModel().PrepareToGridAsync(searchModel, sliderItems, () =>
            {
                return sliderItems.SelectAwait(async sliderItem =>
                {
                    var slider = await _mysliderService.GetSliderByIdAsync(sliderItem.MySlidersId);
                    return await PrepareSliderItemModelAsync(null, slider, sliderItem);
                });
            });

            return model;
        }


        // slider model
        public async Task<MySliderModel> PrepareSliderModelAsync(MySliderModel model, MySliders slider, bool excludeProperties = false)
        {

            if (slider != null)
            {
                if (model == null)
                {
                    model = slider.ToModel<MySliderModel>();
                    
                    model.CatalogPageStr = MySliderHelper.GetCustomCatalogPage(slider.CatalogPageId);
                    model.WidgetZoneStr = MySliderHelper.GetCustomWidgetZone(slider.WidgetZoneId);
                    model.SelectedCustomerRoleIds = _mysliderCustomerService.GetCustomerRoleBySliderId(slider.Id);
                    model.CreatedOn = await _dateTimeHelper.ConvertToUserTimeAsync(slider.CreatedOnUtc, DateTimeKind.Utc);
                    model.UpdatedOn = await _dateTimeHelper.ConvertToUserTimeAsync(slider.UpdatedOnUtc, DateTimeKind.Utc);
                    
                    if (slider.CatalogPageId == 1)
                    {
                        model.ProductWidgetZoneId = slider.WidgetZoneId;
                    }
                    else if (slider.CatalogPageId == 2)
                    {
                        model.CategoryWidgetZoneId = slider.WidgetZoneId;
                    }
                    else
                    {
                        model.ManufactureWidgetZoneId = slider.WidgetZoneId;
                    }
                }
            }

            if (!excludeProperties)
            {
                await PrepareCustomCustomerRolesAsync(model.AvailableCustomerRoles, true);
                //model.SelectedCustomerRoleIds = _mysliderCustomerService.GetCustomerRoleBySliderId(slider.Id);
                
                model.AvailableCatalogPages = await MySliderHelper.GetCustomCatalogPageSelectListAsync();
               // model.AvailableCustomerRoles = await MySliderHelper.GetCustomCustomerRoleSelectListAsync();
               // model.AvailableWidgetZones = await MySliderHelper.GetCustomWidgetZoneSelectListAsync(model.CatalogPageId);
                model.AvailableProductWidgetZones =  MySliderHelper.GetCustomWidgetZoneSelectList(1);
                model.AvailableCategoryWidgetZones =  MySliderHelper.GetCustomWidgetZoneSelectList(2);
                model.AvailableManufactureWidgetZones =  MySliderHelper.GetCustomWidgetZoneSelectList(3);
                await _storeMappingSupportedModelFactory.PrepareModelStoresAsync(model, slider, excludeProperties);
            }
           

            return model;
        }


        // Slider list model
        public async Task<MySliderListModel> PrepareSliderListModelAsync(MySliderSearchModel slidersearchModel)
        {
            if (slidersearchModel == null)
                throw new ArgumentNullException(nameof(slidersearchModel));

            var catalogPageIds = slidersearchModel.SearchCatalogPages?.Contains(0) ?? true ?
              null : slidersearchModel.SearchCatalogPages.ToList();

            var widgetZoneIds = slidersearchModel.SearchWidgetZones?.Contains(0) ?? true ?
                null : slidersearchModel.SearchWidgetZones.ToList();

            bool? active = null;
            if (slidersearchModel.SearchActiveId == 1)
                active = true;
            else if (slidersearchModel.SearchActiveId == 2)
                active = false;

            
            var sliders = await _mysliderService.GetAllSlidersAsync(widgetZoneIds, catalogPageIds,null,slidersearchModel.SearchStoreId,
                active, slidersearchModel.Page - 1, slidersearchModel.PageSize);

            
            var model = await new MySliderListModel().PrepareToGridAsync(slidersearchModel, sliders, () =>
            {
                return sliders.SelectAwait(async slider =>
                {
                    return await PrepareSliderModelAsync(null, slider, true);
                });
            });

            return model;
        }

        // slider search model
        public async Task<MySliderSearchModel> PrepareSliderSearchModelAsync(MySliderSearchModel sliderSearchModel)
        {
            if (sliderSearchModel == null)
                throw new ArgumentNullException(nameof(sliderSearchModel));

            
            await PrepareCustomCatalogPagesAsync(sliderSearchModel.AvailableCatalogPages, true);
            await PrepareCustomWidgetZonesAsync(sliderSearchModel.AvailableWidgetZones, true);
            await PrepareActiveOptionsAsync(sliderSearchModel.AvailableActiveOptions, true);

            await _baseAdminModelFactory.PrepareStoresAsync(sliderSearchModel.AvailableStores);

            return sliderSearchModel;
        }



        private async Task PrepareActiveOptionsAsync(IList<SelectListItem> items, bool withSpecialDefaultItem = true)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            items.Add(new SelectListItem()
            {
                Text = await _localizationService.GetResourceAsync("Nop.MySlider.Sliders.List.SearchActive.Active"),
                Value = "1"
            });
            items.Add(new SelectListItem()
            {
                Text = await _localizationService.GetResourceAsync("Nop.MySlider.Sliders.List.SearchActive.Inactive"),
                Value = "2"
            });

            if (withSpecialDefaultItem)
            {
                items.Insert(0, new SelectListItem()
                {
                    Text = await _localizationService.GetResourceAsync("Admin.Common.All"),
                    Value = "0"
                });
            }
        }

        private async Task PrepareCustomWidgetZonesAsync(IList<SelectListItem> items, bool withSpecialDefaultItem = true)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            var availableWidgetZones = MySliderHelper.GetCustomWidgetZoneSelectList(0);

            foreach (var zone in availableWidgetZones)
            {
                items.Add(zone);
            }

            if (withSpecialDefaultItem)
            {
                items.Insert(0, new SelectListItem()
                {
                    Text = await _localizationService.GetResourceAsync("Admin.Common.All"),
                    Value = "0"
                });
            }
        }

        private async Task PrepareCustomCustomerRolesAsync(IList<SelectListItem> items, bool withSpecialDefaultItem = true)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            var availableCustomerRoles = await MySliderHelper.GetCustomCustomerRoleSelectListAsync();

            foreach (var zone in availableCustomerRoles)
            {
                items.Add(zone);
            }

            if (withSpecialDefaultItem)
            {
                items.Insert(0, new SelectListItem()
                {
                    Text = await _localizationService.GetResourceAsync("Admin.Common.All"),
                    Value = "0"
                });
            }
        }

        private async Task PrepareCustomCatalogPagesAsync(IList<SelectListItem> items, bool withSpecialDefaultItem = true)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            var availableCatalogPages = await MySliderHelper.GetCustomCatalogPageSelectListAsync();

            foreach (var zone in availableCatalogPages)
            {
                items.Add(zone);
            }

            if (withSpecialDefaultItem)
            {
                items.Insert(0, new SelectListItem()
                {
                    Text = await _localizationService.GetResourceAsync("Admin.Common.All"),
                    Value = "0"
                });
            }
        }
    }
}
